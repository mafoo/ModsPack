﻿using System;

namespace GSF.GSFPulseCannons
{
	public class PlasmaWeaponInfo
	{
		public float powerUsage;
		public float energyPerProjectile;

		public string ammoName;
		public int classes;

		public int projectilesPerCharge;
		
		public int keepAtCharge;

		public PlasmaWeaponInfo(float powerUsage, float energyPerProjectile, string ammoName, int classes,  int projectilesPerCharge, int keepAtCharge) {

			this.powerUsage = powerUsage;
			this.energyPerProjectile = energyPerProjectile;
			this.ammoName = ammoName;
			this.classes = classes;
			this.projectilesPerCharge = projectilesPerCharge;
			this.keepAtCharge = keepAtCharge;
		}
	}
}

